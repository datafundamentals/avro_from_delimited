package com.datafundamentals.gdu.avro.exception;

public class BadDataException  extends RuntimeException {
	  public BadDataException(Throwable cause) { super(cause); }
	  public BadDataException(String message) { super(message); }
	  public BadDataException(String message, Throwable cause) {
	    super(message, cause);
	  }
}
