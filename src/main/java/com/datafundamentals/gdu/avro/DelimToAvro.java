package com.datafundamentals.gdu.avro;

import java.io.File;
import java.net.URI;
import java.util.List;

import org.apache.avro.Schema;

import com.datafundamentals.gdu.avro.exception.BadDataException;
import com.datafundamentals.gdu.avro.helper.CompareSchemas;
import com.datafundamentals.gdu.avro.helper.GetColHeads;
import com.datafundamentals.gdu.avro.helper.GetSchema;
import com.datafundamentals.gdu.avro.helper.ToAvro;

public class DelimToAvro {
	ToAvro toAvro = new ToAvro();
	GetColHeads getColHeads = new GetColHeads();
	GetSchema getSchema = new GetSchema();
	CompareSchemas compareSchemas = new CompareSchemas();

	/**
	 * UseCase4
	 * 
	 */
	public String get(URI inputURI, URI schemaURI, File outputFile,
			String delimiter, boolean firstLineColHeads,
			boolean validateSchemaFromFirstLineColHeads,
			boolean exceptionOnBadData) {
		String error = null;
		Schema schema = new GetSchema().fromJsonSchema(schemaURI);
		List<String> colHeads = getColHeads.fromFirstLineOfFile(inputURI,
				delimiter);
		Schema schema2 = getSchema.fromColHeads("mattersnot", "mattersnot", colHeads);
		if (!compareSchemas.haveSameFieldNames(schema, schema2, false)) {
			throw new BadDataException(
					"Schema column names do not match column names in first line of file");
		}
		error = toAvro.write(inputURI, outputFile, schema, delimiter, true,
				exceptionOnBadData);
		return error;
	}

	/**
	 * UseCase3
	 * 
	 */
	public String get(URI inputURI, URI schemaURI, File outputFile,
			String delimiter, boolean exceptionOnBadData) {
		String error = null;
		Schema schema = new GetSchema().fromJsonSchema(schemaURI);
		error = toAvro.write(inputURI, outputFile, schema, delimiter, true,
				exceptionOnBadData);
		return error;
	}

	/**
	 * UseCase2
	 * 
	 */
	public String get(URI inputURI, File outputFile, String delimiter,
			boolean exceptionOnBadData) {
		String error = null;
		Schema schema = new GetSchema().fromFirstLine(inputURI);
		error = toAvro.write(inputURI, outputFile, schema, delimiter, true,
				exceptionOnBadData);
		return error;
	}

	/**
	 * Takes a delimited text file with column heads as first row, and converts
	 * it into an avro file.
	 * 
	 */

	public String get(URI inputURI, File outputFile, String className,
			String namespace, String delimiter, boolean exceptionOnBadData) {
		String error = null;
		List<String> colHeads = getColHeads.fromFirstLineOfFile(inputURI,
				delimiter);
		Schema schema = getSchema.fromColHeads(className, namespace, colHeads);
		error = toAvro.write(inputURI, outputFile, schema, delimiter, true,
				exceptionOnBadData);
		return error;
	}

}
