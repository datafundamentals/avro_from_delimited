package com.datafundamentals.gdu.avro.helper;

import java.util.List;

import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;

public class CompareSchemas {
	public boolean haveSameFieldNames(Schema one, Schema other,
			boolean caseSensitive) {
		boolean haveVirtuallySameFieldNames = true;
		List<Field> a = one.getFields();
		List<Field> b = other.getFields();
		if (a.size() != b.size()) {
			haveVirtuallySameFieldNames = false;
		} else {
			for (int i = 0; i < a.size(); i++) {
				if (!same(a.get(i).name(), b.get(i).name(), caseSensitive)) {
					haveVirtuallySameFieldNames = false;
					break;
				}
			}
		}
		return haveVirtuallySameFieldNames;
	}

	boolean same(String a, String b, boolean caseSensitive) {
		if (a == null || b == null) {
			throw new IllegalArgumentException(
					"Cannot be called with a null field name");
		}
		if (caseSensitive) {
			return a.equals(b);
		} else {
			return a.equalsIgnoreCase(b);
		}
	}

}
