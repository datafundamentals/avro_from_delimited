package com.datafundamentals.gdu.avro.helper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.apache.avro.SchemaBuilder;
import org.codehaus.jackson.node.NullNode;

public class GetSchema {
	public Schema fromColHeads(String className, String namespace,
			List<String> colHeads) {
		final Schema schema = Schema.createRecord(className, null, namespace,
				false);
		final List<Field> fields = new ArrayList<Field>();
		Field field;
		for (String colHead : colHeads) {
			field = new Field(colHead, SchemaBuilder.unionOf().nullType()
					.and().stringType().endUnion(), null, NullNode.getInstance());
			fields.add(field);
		}
		schema.setFields(fields);
		return schema;
	}

	public Schema fromFirstLine(URI input) {
		Schema schema = null;
		BufferedReader reader = new GetReader().fromUri(input);
		try {
			String line = reader.readLine();
			schema = new Schema.Parser().parse(line);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}finally {
			try {
				reader.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return schema;
	}

	public Schema fromJsonSchema(URI jsonSchema) {
		Schema schema = null;
		BufferedReader reader = new GetReader().fromUri(jsonSchema);
		StringBuilder sb = new StringBuilder();
		try {
			String line = reader.readLine();
			while (line != null) {
				sb.append(line);
				line = reader.readLine();
			}
			System.out.println(sb.toString());
			schema = new Schema.Parser().parse(sb.toString());
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return schema;
	}

}
