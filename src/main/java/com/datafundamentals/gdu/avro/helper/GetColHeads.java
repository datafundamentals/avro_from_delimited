package com.datafundamentals.gdu.avro.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;

public class GetColHeads {
	GetReader getReader = new GetReader();
	
	public List<String> fromFirstLineOfFile(URI input, String delimiter) {
		return fromColHeadFile( input,  delimiter);
	}

	public List<String> fromColHeadFile(URI input, String delimiter) {
		BufferedReader reader = getReader.fromUri(input);
		List<String> colHeads = new ArrayList<String>();
		try {
			if (delimiter.equals("\n")) {
				parseLines(reader, colHeads);
			} else {
				String line = reader.readLine();
				colHeads = fromDelimited(line, delimiter);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}finally {
			try {
				reader.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return colHeads;
	}
	

	private void parseLines(BufferedReader in, List<String> colHeads) {
		String line;
		try {
			line = in.readLine();
			while (line != null) {
				colHeads.add(line.trim());
				line = in.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public List<String> fromDelimited(String input, String delimiter) {
		List<String> colHeads = new ArrayList<String>();
		StringTokenizer stk = new StringTokenizer(input, delimiter);
		while (stk.hasMoreElements()) {
			colHeads.add(normalizedString(stk.nextToken()));
		}
		return colHeads;
	}

	private String normalizedString(String string) {
		if (string == null) {
			return null;
		} else {
			return string.trim();
		}
	}
}
