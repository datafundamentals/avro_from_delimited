package com.datafundamentals.gdu.avro.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;

public class InputStreamFromUri {
	public InputStream get(URI uri) {
		InputStream inputStream = null;
		if (uri.getScheme().startsWith("http")) {
			inputStream = getInputStreamFromUrl(uri);
		} else if (uri.getScheme().equals("file")) {
			inputStream = getInputStreamFromFile(uri);
		} else {
			throw new IllegalArgumentException(
					"InputStreamFromUri can only consume a file or http uri");
		}
		return inputStream;
	}

	InputStream getInputStreamFromFile(URI uri) {
		InputStream inputStream = null;
		File file = new File(uri.getPath());
		try {
			inputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Was handed a file path of "
					+ uri.getPath()
					+ " which does not point to an existing file path");
		}
		return inputStream;
	}

	InputStream getInputStreamFromUrl(URI uri) {
		InputStream inputStream = null;
		try {
			inputStream = uri.toURL().openConnection()
					.getInputStream();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Was handed a uri path of "
					+ uri.toString()
					+ " which is malformed");

		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Was handed a uri path of "
					+ uri.getHost()
					+ " which does not work");

		}
		return inputStream;
	}

}
