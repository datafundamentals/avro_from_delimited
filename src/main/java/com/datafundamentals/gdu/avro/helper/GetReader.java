package com.datafundamentals.gdu.avro.helper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

public class GetReader {
	
	BufferedReader fromUri(URI uri){
		BufferedReader bufferedReader = null;
		InputStream inputStream = new InputStreamFromUri().get(uri);
		bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		return bufferedReader;
	}

}
