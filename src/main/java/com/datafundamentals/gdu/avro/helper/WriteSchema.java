package com.datafundamentals.gdu.avro.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.avro.Schema;

public class WriteSchema {
	public void write(File outputFile, Schema schema) throws IOException {
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(getSchemaFilePath(outputFile));
			fileWriter.append(schema.toString(true));
			fileWriter.flush();
		} finally {
			fileWriter.close();
		}
	}

	File getSchemaFilePath(File outputFile) {
		String filePath = outputFile.getAbsolutePath();
		filePath = filePath.substring(0, filePath.lastIndexOf(".")) + ".avsc";
		return new File(filePath);
	}

}
