package com.datafundamentals.gdu.avro.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import com.datafundamentals.gdu.avro.exception.BadDataException;

public class GetGenericRecord {
	public GenericRecord get(Schema schema, String line, String delimiter) {
		GenericRecord genericRecord = new GenericData.Record(schema);
		List<Field> fields = schema.getFields();
		List<Object> data = getData(line, delimiter, fields.size(), schema);
		for (int i = 0; i < fields.size(); i++) {
			genericRecord.put(fields.get(i).name(), data.get(i));
		}
		return genericRecord;
	}

	/*
	 * Improves upon the String split command by setting ending last entries as
	 * null instead of not adding them to the list
	 */
	List<Object> getData(String line, String delimiter, int size, Schema schema) {
		String[] array = line.split(delimiter);
		List<Object> data = new ArrayList<Object>();
		for (int i = 0; i < array.length; i++) {
			Object value = cast(array[i], schema, i);
			data.add(value);
		}
		/* finish up any null fields not pulled in by String.split() */
		for (int i = data.size(); i < size; i++) {
			data.add(null);
		}
		return data;
	}

	/*
	 * Very hackish casting of String to other types. What could possibly go
	 * wrong? boolean: a binary value int: 32-bit signed integer long: 64-bit
	 * signed integer float: single precision (32-bit) IEEE 754 floating-point
	 * number double: double precision (64-bit) IEEE 754 floating-point number
	 * bytes: sequence of 8-bit unsigned bytes string: unicode character
	 * sequence
	 */
	Object cast(String value, Schema schema, int column) {
		String schemaString = schema.getFields().get(column).schema()
				.toString();
		if (value != null) {
			value = value.trim();
		}
		Object object = value;
		if (value.length() == 0) {
			object = null;
		}
		if (object != null && schema != null) {
			try {
				if (schemaString.contains("double")) {
					object = Double.parseDouble(value);
				} else if (schemaString.contains("float")) {
					object = Float.parseFloat(value);
				} else if (schemaString.contains("bytes")) {
					object = value.getBytes();
				} else if (schemaString.contains("long")) {
					object = Long.parseLong(value);
				} else if (schemaString.contains("int")) {
					object = Integer.parseInt(value);
				} else if (schemaString.contains("boolean")) {
					object = Boolean.parseBoolean(value);
				} else if (schemaString.contains("string")) {
					// do nothing, object is already set as string value
				} else {
					throw new BadDataException(
							"could not pick out a primitive from schema "
									+ schemaString
									+ " please inspect the avro schema for the field position "
									+ column);
				}
			} catch (Exception e) {
				throw new BadDataException("With a value of "
						+ object.toString() + " and a field order of " + column
						+ " failed to cast value properly for "+ schema.toString(), e);
			}
		}
		return object;
	}
}
