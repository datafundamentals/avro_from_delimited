package com.datafundamentals.gdu.avro.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.SchemaCompatibility;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.file.DataFileWriter.AppendWriteException;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datafundamentals.gdu.avro.exception.BadDataException;

public class ToAvro {
	private static final Logger LOG = LoggerFactory.getLogger(ToAvro.class);

	@SuppressWarnings("resource")
	public String write(URI uri, File outputFile, Schema schema,
			String delimiter, boolean skipFirst, boolean exceptionOnBadData) {
		StringBuffer errorMessage = new StringBuffer();
		if (schema == null) {
			throw new AvroRuntimeException("Schema may not be null");
		}
		BufferedReader reader = null;
		InputStream inputStream = new InputStreamFromUri().get(uri);
		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(
				schema);
		DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(
				datumWriter);
		GetGenericRecord getGenericRecord = new GetGenericRecord();
		try {
			dataFileWriter.create(schema, outputFile);
			reader = new BufferedReader(new InputStreamReader(inputStream));
			boolean first = true;
			GenericRecord genericRecord = null;
			for (String line; (line = reader.readLine()) != null;) {
				if ((!first || !skipFirst)) {
					try {
						genericRecord = getGenericRecord.get(schema, line,
								delimiter);
						dataFileWriter.append(genericRecord);
					} catch (Exception e) {
						String error = "Bad Line of data: \n\t'" + line
								+ "'\n\t" + e.getMessage() + "\n";
						if (!exceptionOnBadData) {
							errorMessage.append(error);
							LOG.error(error);
						} else {
							close(dataFileWriter);
							throw new BadDataException(error, e);
						}
					}
				}
				first = false;
			}
			new WriteSchema().write(outputFile, schema);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			close(dataFileWriter);
		}
		if (errorMessage.toString().trim().length() > 0) {
			System.err.println(errorMessage.toString());
			return errorMessage.toString().trim();
		} else {
			return null;
		}
	}

	void close(DataFileWriter<GenericRecord> dataFileWriter) {
		try {
			dataFileWriter.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
