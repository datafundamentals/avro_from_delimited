package com.datafundamentals.gdu.avro.helper;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class GetColHeadsTest extends AbstractSchemaTest{

	@Test
	public void testFromFirstLineOfFile() {
		assertTrue(file1.exists());
		assertTrue(validateSame(colHeads1));
	}

	@Test
	public void testFromColHeadFile() {
		assertTrue(validateSame(colHeads4));
	}

	@Test
	public void testFromDelimited() {
		assertTrue(validateSame(colHeads2));
	}

	private boolean validateSame(List<String> colHeadsCompare) {
		boolean isSame = true;
		if (colHeadsCompare.size() != colHeads.size()) {
			isSame = false;
		}
		for (int i = 0; i < colHeadsCompare.size(); i++) {
			if (!colHeads.get(i).toLowerCase().equals(colHeadsCompare.get(i).toLowerCase())) {
				isSame = false;
			}
		}
		return isSame;
	}


}
