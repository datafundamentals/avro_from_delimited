package com.datafundamentals.gdu.avro.helper;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.apache.avro.Schema;
import org.junit.Test;

import com.datafundamentals.gdu.avro.helper.GetSchema;

public class GetSchemaTest extends AbstractSchemaTest{
	String TEST_SCHEMA = "{\"type\":\"record\",\"name\":\"Bar\",\"namespace\":\"com.foo\",\"fields\":[{\"name\":\"colOne\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"ColTwo\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"ColThree\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"COlFour\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"ColFive\",\"type\":[\"null\",\"string\"],\"default\":null}]}";
	String TEST_SCHEMA2 = "{\"type\":\"record\",\"name\":\"MyFoo\",\"namespace\":\"com.foo.stuff\",\"fields\":[{\"name\":\"ColOne\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"colTwo\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"ColThree\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"ColFour\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"ColFive\",\"type\":\"int\"}]}";

	@Test
	public void testFromColHeads() {
		Schema schema = new GetSchema().fromColHeads("Bar", "com.foo", colHeads);
		assertEquals(TEST_SCHEMA, schema.toString());
	}
	
	@Test
	public void testFromFirstLineProvided() {
		File fileHasSchema = new File("./src/test/resources/testFile6.csv");
		Schema schema = new GetSchema().fromFirstLine(fileHasSchema.toURI());
		assertEquals(TEST_SCHEMA2, schema.toString());
	}

}
