package com.datafundamentals.gdu.avro.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.junit.Before;
import org.junit.Test;

import com.datafundamentals.gdu.avro.exception.BadDataException;
import com.datafundamentals.gdu.avro.helper.GetSchema;
import com.datafundamentals.gdu.avro.helper.ToAvro;

public class ToAvroTest extends AbstractSchemaTest {
	ToAvro toAvro = new ToAvro();
	File outputFile = new File("target/test.avro");
	File goodDataFile = new File("./src/test/resources/testFile1.csv");
	File badDataFile = new File("./src/test/resources/testFile5.csv");
	GetSchema getSchema = new GetSchema();
	Schema schema = getSchema.fromColHeads("Foo", "my.foobar", colHeads1);
	String badSchema = "{\"type\":\"record\",\"name\":\"Foo\",\"namespace\":\"my.foobar\",\"fields\":[{\"name\":\"ColOne\",\"type\":[\"int\",\"null\"],\"default\":null},{\"name\":\"colTwo\",\"type\":[\"int\",\"null\"],\"default\":null},{\"name\":\"ColThree\",\"type\":[\"int\",\"null\"],\"default\":null},{\"name\":\"ColFour\",\"type\":[\"int\",\"null\"],\"default\":null},{\"name\":\"ColFive\",\"type\":[\"string\",\"null\"],\"default\":null}]}";

	@Before
	public void setup() {
	}

	@Test
	public void testWriteSkippingFirst() throws Exception {
		assertNull(write(goodDataFile, schema, true, false));
	}

	@Test
	public void testWriteWithoutSkippingFirst() throws Exception {
		assertNull(write(goodDataFile, schema, false, false));
	}

	@Test
	public void testExceptionOnBadDataWriteSkippingFirst() throws Exception {
		Schema aBadSchema = new Schema.Parser().parse(badSchema);
		try {
			write(badDataFile, aBadSchema, true, true);
			fail("should have thrown excpetion before reaching this point");
		} catch (BadDataException e) {
			// expected
		}
	}

	@Test
	public void testEexceptionOnBadDataWriteWithoutSkippingFirst()
			throws Exception {
		Schema aBadSchema = new Schema.Parser().parse(badSchema);
		try {
			write(badDataFile, aBadSchema, false, true);
			fail("should have thrown excpetion before reaching this point");
		} catch (BadDataException e) {
			// expected
		}
	}

	@Test
	public void testNoexceptionOnBadDataWriteWithoutSkippingFirst()
			throws Exception {
		Schema aBadSchema = new Schema.Parser().parse(badSchema);
		assertTrue(write(badDataFile, aBadSchema, false, false).length()>0);
	}

	@Test
	public void testNoexceptionOnBadDataWriteSkippingFirst() throws Exception {
		Schema aBadSchema = new Schema.Parser().parse(badSchema);
		assertTrue(write(badDataFile, aBadSchema, true, false).length()>0);
	}

	String write(File inputFile, Schema schema, boolean skipFirst,
			boolean exceptionOnBadData) throws IOException {
		String error = toAvro.write(inputFile.toURI(), outputFile, schema, ",",
				skipFirst, exceptionOnBadData);
		DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(
				schema);
		DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(
				outputFile, datumReader);
		GenericRecord user = null;
		int i = 0;
		if (skipFirst == false) {
			i = -1;
		}
		while (dataFileReader.hasNext()) {
			user = dataFileReader.next(user);
			if (i == 0) {
				assertEquals(
						user.toString(),
						"{\"ColOne\": \"898sdf\", \"colTwo\": null, \"ColThree\": \"sdf\", \"ColFour\": \"aaaaa\", \"ColFive\": \"89343\"}");
			} else if (i == 1) {
				assertEquals(
						user.toString(),
						"{\"ColOne\": \"13534\", \"colTwo\": \"34352\", \"ColThree\": \"sdf\", \"ColFour\": \"aa\", \"ColFive\": \"89443\"}");
			}
			i++;
		}
		return error;
	}

	@Test
	public void testClose() throws IOException {
		Schema schema = getSchema.fromColHeads("Foo", "my.foobar", colHeads1);
		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(
				schema);
		DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(
				datumWriter);
		toAvro.close(dataFileWriter);
		try {
			dataFileWriter.flush();
			fail("should not have been reachable");
		} catch (AvroRuntimeException e) {
			// expected
		}
	}

}
