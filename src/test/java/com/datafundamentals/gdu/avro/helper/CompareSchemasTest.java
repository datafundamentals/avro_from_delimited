package com.datafundamentals.gdu.avro.helper;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.apache.avro.Schema;
import org.junit.Test;

import com.datafundamentals.gdu.avro.helper.CompareSchemas;
import com.datafundamentals.gdu.avro.helper.GetSchema;

public class CompareSchemasTest  extends AbstractSchemaTest{
	CompareSchemas compareSchemas = new CompareSchemas();
	GetSchema getSchema = new GetSchema();

	@Test
	public void testHaveSameFieldNames() {
		Schema schema1 = getSchema.fromColHeads("Foo", "com.bar", colHeads1);
		Schema schema2 = getSchema.fromColHeads("Foo", "com.bar", colHeads2);
		Schema schema4 = getSchema.fromColHeads("Foo", "com.bar", colHeads4);
		assertTrue(compareSchemas.haveSameFieldNames(schema1, schema2, false));
		assertTrue(compareSchemas.haveSameFieldNames(schema2, schema4, false));
		assertTrue(compareSchemas.haveSameFieldNames(schema1, schema4, false));
		assertFalse(compareSchemas.haveSameFieldNames(schema1, schema2, true));
		assertFalse(compareSchemas.haveSameFieldNames(schema2, schema4, true));
		assertFalse(compareSchemas.haveSameFieldNames(schema1, schema4, true));
	}

	@Test
	public void testSame() {
		assertTrue(compareSchemas.same("abcD", "aBcd", false));
		assertFalse(compareSchemas.same("abcD", "aBcd", true));
	}

}
