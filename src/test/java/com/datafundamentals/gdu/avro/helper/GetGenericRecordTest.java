package com.datafundamentals.gdu.avro.helper;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.junit.Before;
import org.junit.Test;

import com.datafundamentals.gdu.avro.helper.CompareSchemas;
import com.datafundamentals.gdu.avro.helper.GetGenericRecord;
import com.datafundamentals.gdu.avro.helper.GetSchema;

public class GetGenericRecordTest extends AbstractSchemaTest{
	CompareSchemas compareSchemas = new CompareSchemas();
	GetSchema getSchema = new GetSchema();
	Schema schema1 = null;
	GetGenericRecord getGenericRecord = new GetGenericRecord();
	String line = "ColOne ,colTwo, ColThree,ColFour, ColFive";
	List<Object> myColHeads = null;
	@Before
	public void setUp() throws Exception {
		 schema1 = getSchema.fromColHeads("Foo", "com.bar", colHeads1);
		 myColHeads = getGenericRecord.getData(line, ",", 5, schema1);
	}

	@Test
	public void testGet() {
		GenericRecord thisGenericRecord = getGenericRecord.get(schema1, line, ",");
		for(int i=0;i<myColHeads.size();i++){
			assertEquals(myColHeads.get(i), thisGenericRecord.get((String) myColHeads.get(i)));
		}
	}

	@Test
	public void testGetData() {
		for(int i=0;i<myColHeads.size();i++){
			assertEquals(myColHeads.get(i), this.colHeads1.get(i));
		}
		String alt = "ColOne ,colTwo, ,ColFour, ColFive";
		assertEquals(5, getGenericRecord.getData(alt, ",", 5, schema1).size()); 
		alt = "ColOne ,,foo,,";
		assertEquals(5, getGenericRecord.getData(alt, ",", 5, schema1).size());
	}

}
