package com.datafundamentals.gdu.avro.helper;

import static org.junit.Assert.fail;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import com.datafundamentals.gdu.avro.helper.InputStreamFromUri;

public class InputStreamFromUriTest {
	String filePath = "./src/test/resources/testFile1.csv";
	String fullFilePath = "./src/test/resources/testFile1.csv";
	URI webURI = null;
	InputStreamFromUri inputStreamFromUri = new InputStreamFromUri();

	@Before
	public void setup() {
		/*
		 * Had to do some hacks to get a proper URI due to inexperience with
		 * that API, but since this is out of the scope of this program seems
		 * not to matter how obtained for legitimate testing
		 */
		File file = new File(filePath);
		fullFilePath = file.getAbsolutePath();
		try {
			URL url = new URL("https://google.com");
			webURI = url.toURI();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			fail();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testGet() {
	}

	@Test
	public void testGetInputStreamFromFile() {
		URI uri = null;
		try {
			uri = new URI("file:///" + fullFilePath);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		inputStreamFromUri.getInputStreamFromFile(uri);
		try {
			uri = new URI("file:///mynonexesistentfile.txt");
			inputStreamFromUri.getInputStreamFromFile(uri);
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// expected
		}
	}

	@Test
	public void testGetInputStreamFromUrl() {
		inputStreamFromUri.getInputStreamFromUrl(webURI);
		try {
			webURI = new URI("https", "google.com",
					"/calendar/renderationsiiictic", "");
			inputStreamFromUri.getInputStreamFromFile(webURI);
			fail();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// expected
		}
	}

}
