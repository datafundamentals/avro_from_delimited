package com.datafundamentals.gdu.avro;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import com.datafundamentals.gdu.avro.DelimToAvro;

public class DelimToAvroTest {

	@Before
	public void setUp() throws Exception {

	}

	/*
	 * columnHeaderInternal
	 */
	@Test
	public void testUseCase1() {
		DelimToAvro delimToAvro = new DelimToAvro();
		File targetFile = new File("target/deleteme.avro");
		File inputFile = new File("./src/test/resources/testFile1.csv");
		delimToAvro.get(inputFile.toURI(), targetFile, "MyFoo",
				"com.foo.stuff", ",", false);
	}

	/*
	 * schemaInternal
	 */
	@Test
	public void testUseCase2() {
		DelimToAvro delimToAvro = new DelimToAvro();
		File targetFile = new File("target/deleteme.avro");
		File inputFile = new File("./src/test/resources/testFile6.csv");
		delimToAvro.get(inputFile.toURI(), targetFile, ",", false);
	}

//	schemaExternal
	@Test
	public void testUseCase3() {
		DelimToAvro delimToAvro = new DelimToAvro();
		File targetFile = new File("target/deleteme.avro");
		File inputFile = new File("./src/test/resources/testFile3.csv");
		File schemaFile = new File("./src/test/resources/testFile7.avsc");
		delimToAvro.get(inputFile.toURI(), schemaFile.toURI(), targetFile, ",",
				false);
	}

	/*
	 * schemaExternalValidating
	 */
	@Test
	public void testUseCase4() {
		DelimToAvro delimToAvro = new DelimToAvro();
		File targetFile = new File("target/deleteme.avro");
		File inputFile = new File("./src/test/resources/testFile1.csv");
		File schemaFile = new File("./src/test/resources/testFile7.avsc");
		delimToAvro.get(inputFile.toURI(), schemaFile.toURI(), targetFile, ",",
				true, true, false);
	}

}
