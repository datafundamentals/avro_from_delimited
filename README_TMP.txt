Are Delimited Files Serious About Structured Data?
	- not type-able
	- does not mind null values
	
Avro is mismatched, in that it is typed, and does not allow for null values

What this attempts to do
- delimited files are in many ways less than serious about being structured data. This program doesn't attempt to be any more serious than delimited files themselves
- attempts to make sure there is a one to one relationship between column headers and columns
- if instructed, validates that a column names are the same (except not case sensitive)
- creates every column as nullable string, unless given a more specific schema to begin with
- trims every string
What this does NOT attempt to do
- handle ints, etc (easily could, but did not implement this)
- handle delimiters that are /r/n
- handle /n delimited files that have empty lines in the middle
- accept a field length and a record length that are different
- log bad data error or fail on line with different number of fields than the default schema. You can still acheive this outcome, but ONLY providing your own schema, or modifying your own
- clean up lousy exception handling. Some horrific stuff in here. Hopefully it won't ding you.

{
    "namespace":"<%= @answers.job_name %>",
    "name":"<%= @answers.table_name %>",
    "type":"record",
    "fields":[
<%= @answers.schema_body_content %>
    ]
}


    val = ''
    val <<  "        {\"name\":\""
    val << col
    val << "\",\"type\":[\"null\",\"string\"],\"defaultValue\":null},"